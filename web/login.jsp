<%-- 
    Document   : login
    Created on : 28/04/2018, 18:04:28
    Author     : Walter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <style>
            form{
                position:fixed;
                top:25%;
                left:40%;
                
            }
        </style>
    </head>
    <body>
        <form action="ControladoraServlet?action=login"
              method="POST" >
            <h1>Login</h1><br/>
            <a><span class="label label-info">Digite seus dados para iniciar a sessão.</span><a>
            <div class="container">
                <label for="email">E-mail: </label><input type="email" name="email" /><br/>
                <label for="senha">Senha: </label><input type="password" name="senha" /><br/>
                <input type="submit" class="btn" value="Login" />
                <input type="reset" class="btn" value="Limpar" />
                <div class="login-help">
                    <a href="#">Esqueci minha senha</a></br>
                </div>
            </div>
        </form>
        <a><c:out value="${usuarioLogado}"/></a>
    </body>
</html>
