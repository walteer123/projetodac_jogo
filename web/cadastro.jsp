<%-- 
    Document   : cadastro
    Created on : 19/05/2018, 14:28:10
    Author     : Walter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastro</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <style>
            form{
                position:fixed;
                top:20%;
                left:40%;

            }
        </style>
    </head>
    <body>
        <main>
            <form action="ControladoraServlet?action=cadastro"
              method="POST" >
                <div class="form-group">
                    <h1>Cadastro</h2>
                </div>
                <div class="form-group">
                    <label for="inputEmail1">Endereço de email</label>
                    <input type="email" name="email" type="email" class="form-control" id="inputEmail1"  placeholder="Digite seu email" required>                  
                </div>
                <div class="form-group">
                    <label for="inputNickname">Apelido</label>
                    <input name="apelido" type="text" class="form-control" id="inputNickname" placeholder="Apelido" required>
                </div>
                <div class="form-group">
                    <label for="inputPassword1">Senha</label>
                    <input name="senha" type="password" class="form-control" id="inputPassword1" placeholder="Senha" required>
                </div>
                <button type="submit" class="btn btn-primary">Cadastrar</button>
            </form>
        </main>
    </body>
</html>
