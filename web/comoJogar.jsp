<%-- 
    Document   : portal
    Created on : 28/04/2018, 18:02:12
    Author     : Avell B155 FIRE
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page errorPage="erro.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Game One</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <style>
            @import url('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css');
            html,
            body {
              height: 100%
            }
            footer {
                position: fixed;
                /*height: 450px;*/
                bottom: 0;
                width: 100%;
                text-align: center;
            }
        </style>
        <script type="text/javascript" >
                $(document).ready(function() {
                    $(".cadastro").click(function() {
                        $(location).attr('href', 'ControladoraServlet?action=cadastroPage')
                    });
                    $(".login").click(function() {
                        $(location).attr('href', 'ControladoraServlet?action=loginPage')
                    });
                });
        </script>
    </head>
    <body>
        <%--MENU--%>
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-topXXX" style="padding-right: 350px;">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="navbar-brand" href="index.jsp">Game One</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="index.jsp">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ControladoraServlet?action=jogarPage">Jogar</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ControladoraServlet?action=regrasPage">Como Jogar</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ControladoraServlet?action=estatisticasPage">Estatísticas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ControladoraServlet?action=friendzonePage">Friendzone</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ControladoraServlet?action=ajudaPage">Ajuda</a>
                </li>
                <div style="padding-right:10px;"><button class="btn cadastro" type="submit">Cadastre-se</button></div>
                <div style="padding-right:10px;"><button class="btn login" type="submit">Login</button></div>
            </ul>
        </nav>
        
<div class="container">
    <div class="row">
        <div style="padding:20px 0px 20px 0px;">
            <c:forEach items="${regrasLista}" var="regra">
                <h5>${regra.titulo}</h5>
                <p>${regra.conteudo}<p>
            </c:forEach>
        </div>
    </div>
</div>
        
        <!--Footer-->
        <footer class="page-footer font-small stylish-color-dark pt-4 mt-4">

            <!--Footer Links-->
            <div class="container text-center text-md-left">
                <div class="row" style="background-color: lightgray;">

                    <hr class="clearfix w-100 d-md-none">

                    <!--Second column-->
                    <div class="col-md-2 mx-auto">
                        <ul class="list-unstyled">
                            <li>
                                <a href="index.jsp">Home</a>
                            </li>
                            <li>
                                <a href="ControladoraServlet?action=jogarPage">Jogar</a>
                            </li>
                            <li>
                                <a href="ControladoraServlet?action=regrasPage">Como Jogar</a>
                            </li>
                            <li>
                                <a href="ControladoraServlet?action=ajudaPage">Ajuda</a>
                            </li>
                        </ul>
                    </div>
                    <!--/.Second column-->

                    <hr class="clearfix w-100 d-md-none">

                    <!--Third column-->
                    <div class="col-md-2 mx-auto">
                        <ul class="list-unstyled">
                            <li>
                                <a href="ControladoraServlet?action=friendzonePage">Friendzone</a>
                            </li>
                            <li>
                                <a href="#!">Pesquisar</a>
                            </li>
                            <li>
                                <a href="#!">Termos e Condições</a>
                            </li>
                            <li>
                                <a href="#!">Política de Privacidade</a>
                            </li>
                        </ul>
                    </div>
                    <!--/.Third column-->

                    <hr class="clearfix w-100 d-md-none">

                    <!--Fourth column-->
                    <div class="col-md-4">
                        <ul class="list-unstyled">
                            <li>
                                Receba novidades por email!
                            </li>
                            <li>
                                <input type="text">
                            </li>
                            <li style="padding-top: 5px;">
                                <button class="btn cadastro" type="submit">Inscreva-me</button>
                            </li>
                        </ul>
                    </div>
                    <!--/.Fourth column-->
                </div>
            </div>   

        </footer>
        <!--/.Footer-->
    </body>
</html>
