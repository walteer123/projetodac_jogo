/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Utils.Utilitarios;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Ajuda;
import model.Estatistica;
import model.Facade;
import model.Noticia;
import model.Usuarios;

/**
 *
 * @author Walter
 */
@WebServlet(name = "ControladoraServlet", urlPatterns = {"/ControladoraServlet"})
public class ControladoraServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String action = request.getParameter("action");
        RequestDispatcher rd;
        switch(action){
            case "login":
                String email = request.getParameter("email");
                String senha = Utilitarios.getMD5(request.getParameter("senha"));
                Usuarios u = Facade.loginUsuario(email, senha);
                if(u.ehValido()){
                    //usuario eh valido
                    HttpSession session =request.getSession(true);
                    session.setAttribute("usuarioLogado", u);
                    response.sendRedirect("index.jsp");
                    
                    break;
                }
                break;
            case "cadastroPage":
                rd = getServletContext().getRequestDispatcher("/cadastro.jsp");
                
                rd.forward(request, response);
                break;
            case "cadastro":
                String emailCadastro = request.getParameter("email");
                String senhaCadastro = Utilitarios.getMD5(request.getParameter("senha"));
                String apelidoCadastro = request.getParameter("apelido");
                Usuarios us = new Usuarios();
                us.setEmail(emailCadastro);
                us.setSenha(senhaCadastro);
                us.setApelido(apelidoCadastro);
                Facade.inserirUsuario(us);
                response.sendRedirect("portal.jsp");
                break;
            case "home":
                {
                    try {
                        List<Noticia> noticias = Facade.buscaUltimasNoticias();
                        rd = getServletContext().getRequestDispatcher("/portal.jsp");
                        request.setAttribute("noticias", noticias);
                        rd.forward(request, response);
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                }
                break;
            case "loginPage":
                rd = getServletContext().getRequestDispatcher("/login.jsp");
                
                rd.forward(request, response);
                break;
            case "noticia":
                Integer noticiaId = Integer.parseInt(request.getParameter("id"));
                Noticia noticia = Facade.buscaNoticia(noticiaId);                
                rd = getServletContext().getRequestDispatcher("/noticia.jsp");
                request.setAttribute("noticia", noticia);
                rd.forward(request, response);
                break;
            case "noticias":
                try {
                    List<Noticia> noticias = Facade.ListaNoticias();
                    rd = getServletContext().getRequestDispatcher("/noticias.jsp");
                    request.setAttribute("noticias", noticias);
                    rd.forward(request, response);
                } catch (Exception ex) {
                    System.out.println(ex);
                }
                break;
            case "ajudaPage":
                rd = getServletContext().getRequestDispatcher("/ajudaListar.jsp");
                List<Ajuda> ajudaLista;
                ajudaLista = Facade.buscarTodasAjudas();
                request.setAttribute("ajudaLista", ajudaLista);
                rd.forward(request, response);
                break;
            case "regrasPage":
                rd = getServletContext().getRequestDispatcher("/comoJogar.jsp");
                List<Ajuda> regrasLista;
                regrasLista = Facade.buscarTodasRegras();
                request.setAttribute("regrasLista", regrasLista);
                rd.forward(request, response);
                break;   
            case "estatisticasPage":
                HttpSession session = request.getSession();
                Usuarios login = new Usuarios();
                login = (Usuarios) session.getAttribute("usuarioLogado");
                if(login==null){
                    rd = getServletContext().getRequestDispatcher("/login.jsp");
                    rd.forward(request, response);
                }
                else{
                    Estatistica estatistica = Facade.buscaEstatistica(login.getId());
                    rd = getServletContext().getRequestDispatcher("/estatisticas.jsp");
                    request.setAttribute("estatisticas", estatistica);
                    rd.forward(request, response);
                }
                break;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
