/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dao.AjudaDAO;
import dao.EstatisticaDAO;
import dao.NoticiaDAO;
import dao.PersonagemDAO;
import dao.UsuariosDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Avell B155 FIRE
 */
public class Facade {   
    public static void inserirUsuario(Usuarios u) throws IOException{
        UsuariosDAO usuariosDAO = new UsuariosDAO();
        usuariosDAO.inserirUsuario(u);
    }
    
    public static void alterarUsuario(Usuarios u) throws IOException{
        UsuariosDAO usuariosDAO = new UsuariosDAO();
        usuariosDAO.atualizarUsuario(u);
    }
    
    public static Usuarios buscarUsuario(int id) throws IOException{
     UsuariosDAO usuariosDAO = new UsuariosDAO();
     Usuarios usuario = usuariosDAO.consultarUsuario(id);
     return usuario;
    }    
    
    public static void removerUsuario(int id) throws IOException{
     UsuariosDAO usuariosDAO = new UsuariosDAO();
     usuariosDAO.removerUsuario(id);
    }    

    public static List<Usuarios> buscarTodosUsuarios(){
        UsuariosDAO usuariosDAO = new UsuariosDAO();
        List<Usuarios> listaUsuarios = new ArrayList<>();
        try {
           listaUsuarios = usuariosDAO.listarUsuarios();
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return listaUsuarios;       
    }
    
    public static Usuarios loginUsuario(String login, String senha) throws IOException{
        Usuarios u = new Usuarios();
        UsuariosDAO usuariosDAO = new UsuariosDAO();
        try{
            u = usuariosDAO.autenticarUsuario(login, senha);
        } catch(IOException ex){
            System.out.println(ex);
        }
        return u;
    }
    
    public static List<Ajuda> buscarTodasAjudas(){
        AjudaDAO ajudaDAO = new AjudaDAO();
        List<Ajuda> listaAjuda = new ArrayList<>();
        try {
            listaAjuda = ajudaDAO.listarAjuda();
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return listaAjuda;
    }
    
    public static Noticia buscaNoticia(Integer id){
        NoticiaDAO dao = new NoticiaDAO();
        Noticia noticia = new Noticia();
        try {
            noticia = dao.buscaNoticia(id);
        } catch (IOException ex) {
            System.out.println(ex);
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return noticia;
    }
    
    public static List<Noticia> buscaUltimasNoticias() throws IOException, SQLException{
        List<Noticia> noticias = new ArrayList();
        NoticiaDAO dao = new NoticiaDAO();
        noticias = dao.buscaUltimasNoticias();
        Noticia video = dao.buscaUltimoVideo();
        noticias.add(video);
        return noticias;
    };
    
    public static List<Noticia> ListaNoticias() throws IOException, SQLException{
        List<Noticia> noticias = new ArrayList();
        NoticiaDAO dao = new NoticiaDAO();
        noticias = dao.ListaNoticias();
        return noticias;
    };    

    public static List<Ajuda> buscarTodasRegras(){
        AjudaDAO ajudaDAO = new AjudaDAO();
        List<Ajuda> listaRegras = new ArrayList<>();
        try {
            listaRegras = ajudaDAO.listarRegra();
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return listaRegras;
    }

    public static Estatistica buscaEstatistica(Integer id){
        EstatisticaDAO dao = new EstatisticaDAO();
        Estatistica estatistica = new Estatistica();
        try {
            estatistica = dao.buscaEstatistica(id);
            estatistica = dao.buscaRanking(estatistica);
        } catch (IOException ex) {
            System.out.println(ex);
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return estatistica;
    }
    
    public static void inserirPersonagem(Personagem p) throws IOException{
        PersonagemDAO personagemDAO = new PersonagemDAO();
        personagemDAO.inserirPersonagem(p);
    }
    
    public static void alterarPersonagem(Personagem p) throws IOException{
        PersonagemDAO personagemDAO = new PersonagemDAO();
        personagemDAO.atualizarPersonagem(p);
    }
    
    public static Personagem buscarPersonagem(int id) throws IOException{
     PersonagemDAO personagemDAO = new PersonagemDAO();
     Personagem personagem = personagemDAO.consultarPersonagem(id);
     return personagem;
    }    
    
    public static void removerPersonagem(int id) throws IOException{
     PersonagemDAO personagemDAO = new PersonagemDAO();
     personagemDAO.removerPersonagem(id);
    }    

    public static List<Personagem> buscarTodosPersonagens(){
        PersonagemDAO personagemDAO = new PersonagemDAO();
        List<Personagem> listaPersonagens = new ArrayList<>();
        try {
           listaPersonagens = personagemDAO.listarPersonagens();
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return listaPersonagens;       
    }    
    
}
