/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import model.Noticia;

/**
 *
 * @author Walter
 */
public class NoticiaDAO {
    public Noticia buscaUltimoVideo() throws IOException, SQLException {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            Calendar cal = Calendar.getInstance();
            String sql = "select * from novidades where e_video = 'S' order by data_novidade desc limit 1";
            connection = new ConnectionFactorySemProperties().getConnection();
            stmt = connection.prepareStatement(sql);
            rs = stmt.executeQuery();            
            Noticia videoNews = new Noticia();
            if(rs.next()){                
                videoNews.setId(rs.getInt("id_novidade"));
                videoNews.setTitulo(rs.getString("titulo_novidade"));
                videoNews.setConteudo(rs.getString("conteudo_novidade"));
                cal.setTime(rs.getDate("data_novidade"));
                videoNews.setData(cal);
                videoNews.setVideo(rs.getString("e_video"));
            } 
            return videoNews;
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar usuario no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            };
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            };
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            };
        }
    }
    
    public List<Noticia> buscaUltimasNoticias() throws IOException, SQLException {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            Calendar cal = Calendar.getInstance();
            String sql = "select id_novidade, e_video, titulo_novidade, data_novidade, conteudo_novidade, substr(conteudo_novidade, 1, 150) as resumo from novidades where e_video = 'N' order by data_novidade desc limit 3";
            connection = new ConnectionFactorySemProperties().getConnection();
            stmt = connection.prepareStatement(sql);
            rs = stmt.executeQuery(); 
            List<Noticia> noticias = new ArrayList();
            while(rs.next()){
                Noticia News = new Noticia();
                News.setId(rs.getInt("id_novidade"));
                News.setTitulo(rs.getString("titulo_novidade"));
                News.setConteudo(rs.getString("conteudo_novidade"));
                News.setResumo(rs.getString("resumo"));
                cal.setTime(rs.getDate("data_novidade"));
                News.setData(cal);
                News.setVideo(rs.getString("e_video"));
                noticias.add(News);
            } 
            return noticias;
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar usuario no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            };
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            };
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            };
        }
    }

    public Noticia buscaNoticia(Integer id) throws IOException, SQLException {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            Calendar cal = Calendar.getInstance();
            String sql = "select * from novidades where id_novidade = ?";
            connection = new ConnectionFactorySemProperties().getConnection();
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            rs = stmt.executeQuery(); 
            Noticia News = new Noticia();
            if(rs.next()){
                News.setId(rs.getInt("id_novidade"));
                News.setTitulo(rs.getString("titulo_novidade"));
                News.setConteudo(rs.getString("conteudo_novidade"));
                cal.setTime(rs.getDate("data_novidade"));
                News.setData(cal);
                News.setVideo(rs.getString("e_video"));
            } 
            return News;
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar usuario no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            };
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            };
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            };
        }
    }

    public List<Noticia> ListaNoticias() throws IOException, SQLException {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            Calendar cal = Calendar.getInstance();
            String sql = "select id_novidade, e_video, titulo_novidade, data_novidade, conteudo_novidade, substr(conteudo_novidade, 1, 150) as resumo from novidades order by data_novidade desc";
            connection = new ConnectionFactorySemProperties().getConnection();
            stmt = connection.prepareStatement(sql);
            rs = stmt.executeQuery(); 
            List<Noticia> noticias = new ArrayList();
            while(rs.next()){
                Noticia News = new Noticia();
                News.setId(rs.getInt("id_novidade"));
                News.setTitulo(rs.getString("titulo_novidade"));
                News.setConteudo(rs.getString("conteudo_novidade"));
                News.setResumo(rs.getString("resumo"));
                cal.setTime(rs.getDate("data_novidade"));
                News.setData(cal);
                News.setVideo(rs.getString("e_video"));
                noticias.add(News);
            } 
            return noticias;
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar usuario no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            };
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            };
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            };
        }
    }

}
