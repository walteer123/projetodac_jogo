/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Personagem;

/**
 *
 * @author Ciro
 */
public class PersonagemDAO {
    private final String stmtConsultar = "SELECT id_personagem, nome, avatar, apelido FROM personagem WHERE id_personagem = ?";
    private final String stmtRemover = "DELETE FROM personagem WHERE id_personagem = ?";
    private final String stmtListar = "SELECT id_personagem, nome, avatar, apelido FROM personagem";
    private final String stmtAtualizar = "UPDATE personagem SET nome = ?, avatar = ?, apelido = ? WHERE id_personagem = ?";
    private final String stmtInserir = "INSERT INTO personagem (nome, avatar, apelido) values (?, ?, ?)";
    
    public Personagem consultarPersonagem(int id) throws IOException {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            connection = new ConnectionFactorySemProperties().getConnection();
            stmt = connection.prepareStatement(stmtConsultar);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            Personagem personagem;
            personagem = new Personagem();
            if(rs.next()){
                personagem.setId(rs.getInt("id_personagem"));
                personagem.setNome(rs.getString("nome"));
                personagem.setNome(rs.getString("avatar"));
                personagem.setApelido(rs.getString("apelido"));
            } 
            return personagem;
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar personagem no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            };
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            };
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            };
        }
    }
    
    public void removerPersonagem(int id) throws IOException {
        Connection connection = null;
        PreparedStatement stmt = null;
        try{
            connection = new ConnectionFactorySemProperties().getConnection();
            stmt = connection.prepareStatement(stmtRemover);
            stmt.setInt(1, id);
            stmt.execute();
            stmt.close();
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao remover personagem do banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            }
        }
    }        
    
    public List<Personagem> listarPersonagens() throws Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Personagem> lista = new ArrayList<>();
        try{
            con = new ConnectionFactorySemProperties().getConnection();
            stmt = con.prepareStatement(stmtListar);
            rs = stmt.executeQuery();
            while(rs.next()){
                Personagem personagem;
                personagem = new Personagem();
                personagem.setId(rs.getInt("id_personagem"));
                personagem.setNome(rs.getString("nome"));
                personagem.setAvatar(rs.getString("avatar"));
                personagem.setApelido(rs.getString("apelido"));
                lista.add(personagem);
            }
            return lista;
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar a lista de personagens. Origem="+ex.getMessage());
        }finally{
            try{
                rs.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            }
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                con.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            }               
        }
    }
    
    public void atualizarPersonagem(Personagem personagem) throws IOException {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            connection = new ConnectionFactorySemProperties().getConnection();
            stmt = connection.prepareStatement(stmtAtualizar);
            stmt.setString(1, personagem.getNome());
            stmt.setString(2, personagem.getAvatar());
            stmt.setString(3, personagem.getApelido());
            stmt.setInt(4, personagem.getId());
            stmt.execute();
            stmt.close();
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao atualizar personagem no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            }
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            }
        }
    }

    public void inserirPersonagem(Personagem personagem) throws IOException {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            connection = new ConnectionFactorySemProperties().getConnection();
            stmt = connection.prepareStatement(stmtInserir);
            stmt.setString(1, personagem.getNome());
            stmt.setString(2, personagem.getAvatar());
            stmt.setString(3, personagem.getApelido());
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao inserir personagem no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            };
        }
    }
}
