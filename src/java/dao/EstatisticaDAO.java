/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import model.Estatistica;

public class EstatisticaDAO {
    public Estatistica buscaEstatistica(Integer id) throws IOException, SQLException {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            String sql = "select * from estatisticas where id_usuario = ?";
            connection = new ConnectionFactorySemProperties().getConnection();
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            rs = stmt.executeQuery(); 
            Estatistica stats = new Estatistica();
            if(rs.next()){
                stats.setIdUsuario(rs.getInt("id_usuario"));
                stats.setPartidasJogadas(rs.getInt("partidas_jogadas"));
                stats.setPartidasAbandonadas(rs.getInt("partidas_abandonadas"));
                stats.setPartidasVencidas(rs.getInt("partidas_vencidas"));
                stats.setPartidasPerdidas(rs.getInt("partidas_perdidas"));
                stats.setTempoJogado(rs.getInt("tempo_jogado"));
            } 
            return stats;
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar usuario no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            };
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            };
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            };
        }
    }

    public void insertEstatistica(Integer id) throws IOException, SQLException {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            String sql = "insert into estatistica (id_usuario) values(?)";
            connection = new ConnectionFactorySemProperties().getConnection();
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.execute();
            stmt.close();
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar usuario no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            };
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            };
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            };
        }
    }

    public void atualizarEstatistica(Estatistica stats) throws IOException {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            String sql = "update estatistica set "
                       + "partidas_jogadas = ?, "
                       + "partidas_abandonadas = ?, "
                       + "partidas_vencidas = ?, "
                       + "partidas_perdidas = ?, "
                       + "tempo_jogado = ? "
                       + "where id_usuario = ?";
            connection = new ConnectionFactorySemProperties().getConnection();
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, stats.getPartidasJogadas());
            stmt.setInt(2, stats.getPartidasAbandonadas());
            stmt.setInt(3, stats.getPartidasVencidas());
            stmt.setInt(4, stats.getPartidasPerdidas());
            stmt.setInt(5, stats.getTempoJogado());
            stmt.execute();
            stmt.close();
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar contato no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            }
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            }
        }
    }

    public Estatistica buscaRanking(Estatistica stats) throws IOException, SQLException {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            String sql = "select x.rank "
                       + "from "
                       + "("
                              + "select e.*, @rownum := @rownum + 1 as rank "
                              + "from estatisticas e, (select @rownum := 0) r "
                              + "order by e.partidas_vencidas desc, e.partidas_abandonadas "
                       + ") x "
                       + "where x.id_usuario = ? "
                       + "limit 1";
            connection = new ConnectionFactorySemProperties().getConnection();
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, stats.getIdUsuario());
            rs = stmt.executeQuery(); 
            if(rs.next()){
                stats.setRank(rs.getInt("rank"));
            } 
            return stats;
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar usuario no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            };
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            };
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            };
        }
    }
    
}
