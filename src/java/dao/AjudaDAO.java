/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.io.IOException;
import java.sql.Blob;
import model.Ajuda;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ciro
 */
public class AjudaDAO {
    private final String stmtListar = "SELECT id, titulo, conteudo FROM ajuda where tipo = 'A'";
    private final String stmtListarRegra = "SELECT id, titulo, conteudo FROM ajuda where tipo = 'R'";
    
    public List<Ajuda> listarAjuda() throws Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            con = new ConnectionFactorySemProperties().getConnection();
            stmt = con.prepareStatement(stmtListar);
            rs = stmt.executeQuery();
            List<Ajuda> listaAjuda = new ArrayList<>();
            while(rs.next()){
                Ajuda ajuda = new Ajuda();
                ajuda.setId(rs.getInt("id"));
                ajuda.setTitulo(rs.getString("titulo"));
                Blob blob = rs.getBlob("conteudo");
                String conteudo = new String(blob.getBytes(1, (int) blob.length()));
                ajuda.setConteudo(conteudo);
                listaAjuda.add(ajuda);
            }
            return listaAjuda;
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar Ajuda. Origem="+ex.getMessage());
        }finally{
            try{
                rs.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            }
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                con.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            }               
        }
    }
    
    public List<Ajuda> listarRegra() throws Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            con = new ConnectionFactorySemProperties().getConnection();
            stmt = con.prepareStatement(stmtListarRegra);
            rs = stmt.executeQuery();
            List<Ajuda> listaAjuda = new ArrayList<>();
            while(rs.next()){
                Ajuda ajuda = new Ajuda();
                ajuda.setId(rs.getInt("id"));
                ajuda.setTitulo(rs.getString("titulo"));
                Blob blob = rs.getBlob("conteudo");
                String conteudo = new String(blob.getBytes(1, (int) blob.length()));
                ajuda.setConteudo(conteudo);
                listaAjuda.add(ajuda);
            }
            return listaAjuda;
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar Ajuda. Origem="+ex.getMessage());
        }finally{
            try{
                rs.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            }
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                con.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            }               
        }
    }    
    
}
