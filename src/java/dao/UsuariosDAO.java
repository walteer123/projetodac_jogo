/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Usuarios;

/**
 *
 * @author Ciro
 */
public class UsuariosDAO {
    private final String stmtAutenticar = "SELECT id, email, apelido FROM usuarios WHERE email = ? and senha = ?";
    private final String stmtConsultar = "SELECT id, email, apelido FROM usuarios WHERE id = ?";
    private final String stmtRemover = "DELETE FROM usuarios WHERE id = ?";
    private final String stmtListar = "SELECT id, email, apelido FROM usuarios";
    private final String stmtAtualizar = "UPDATE usuarios SET email = ?, senha = ?, apelido = ? WHERE id = ?";
    private final String stmtInserir = "INSERT INTO usuarios (email, senha, apelido) values (?, ?, ?)";
    
    public Usuarios autenticarUsuario(String login, String senha) throws IOException {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            connection = new ConnectionFactorySemProperties().getConnection();
            stmt = connection.prepareStatement(stmtAutenticar);
            stmt.setString(1, login);
            stmt.setString(2, senha);
            rs = stmt.executeQuery();
            Usuarios usuario = new Usuarios();
            
            if(rs.next()){                
                usuario.setId(rs.getInt("id"));
                usuario.setEmail(rs.getString("email"));
                usuario.setApelido(rs.getString("apelido"));
                usuario.setValido(true);
            } 
            return usuario;
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar contato no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){ 
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            }
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            }
        }
    }
    

    public Usuarios consultarUsuario(int id) throws IOException {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            connection = new ConnectionFactorySemProperties().getConnection();
            stmt = connection.prepareStatement(stmtConsultar);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            Usuarios usuario;
            usuario = new Usuarios();
            if(rs.next()){
                usuario.setId(rs.getInt("id"));
                usuario.setEmail(rs.getString("email"));
                usuario.setApelido(rs.getString("apelido"));
            } 
            return usuario;
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar usuario no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            };
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            };
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            };
        }
    }
    
    public void removerUsuario(int id) throws IOException {
        Connection connection = null;
        PreparedStatement stmt = null;
        try{
            connection = new ConnectionFactorySemProperties().getConnection();
            stmt = connection.prepareStatement(stmtRemover);
            stmt.setInt(1, id);
            stmt.execute();
            stmt.close();
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar usuario no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            }
        }
    }        
    
    public List<Usuarios> listarUsuarios() throws Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Usuarios> lista = new ArrayList<>();
        try{
            con = new ConnectionFactorySemProperties().getConnection();
            stmt = con.prepareStatement(stmtListar);
            rs = stmt.executeQuery();
            while(rs.next()){
                Usuarios usuario;
                usuario = new Usuarios();
                usuario.setId(rs.getInt("id"));
                usuario.setEmail(rs.getString("email"));
                usuario.setApelido(rs.getString("apelido"));
                lista.add(usuario);
            }
            return lista;
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar a lista de usuarios. Origem="+ex.getMessage());
        }finally{
            try{
                rs.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            }
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                con.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            }               
        }
    }
    
    public void atualizarUsuario(Usuarios usuario) throws IOException {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            connection = new ConnectionFactorySemProperties().getConnection();
            stmt = connection.prepareStatement(stmtAtualizar);
            stmt.setString(1, usuario.getEmail());
            stmt.setString(2, usuario.getSenha());
            stmt.setString(3, usuario.getApelido());
            stmt.setInt(4, usuario.getId());
            stmt.execute();
            stmt.close();
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar contato no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            }
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            }
        }
    }

    public void inserirUsuario(Usuarios usuario) throws IOException {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            connection = new ConnectionFactorySemProperties().getConnection();
            stmt = connection.prepareStatement(stmtInserir);
            stmt.setString(1, usuario.getEmail());
            stmt.setString(2, usuario.getSenha());
            stmt.setString(3, usuario.getApelido());
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao inserir usuario no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            };
        }
    }
}
